# TK-D12

Tugas Kelompok untuk PPW kelas D kelompok D12

Anggota kelompok:
- Abdurrahman Arfan
- Alif Iqbal Hazairin
- Dwi Ayu Sekarini Pandjaitan
- Naufal Sani
- Zalfa Luqyana Akifah

Link herokuapp: http://tk1-d12.herokuapp.com

Deskripsi Aplikasi:
Aplikasi ini digunakan sebagai wadah yang menampung para dokter untuk dapat membantu calon pasien dengan cara online. Yang dilakukan di aplikasi yang ini adalah menghubungkan dokter dengan konsumen dengan memesan jadwal konsultasi dengan dokter.
Selain itu, di aplikasi ini, akan ada artikel-artikel yang berkaitan dengan dunia kesehatan, terutama COVID-19 yang menjadi topik utama tahun ini.


Daftar Fitur dalam Aplikasi:
- Saran/masukan web, oleh Zalfa Luqyana Akifah. Textbox dalam halaman utama dapat digunakan user untuk mengirim masukan dalam pengembangan aplikasi.
- List Dokter, oleh Naufal Sani. Menampilkan list dokter-dokter. Dokter ditambahkan melalui admin.
- Detail Dokter, oleh Dwi Ayu Sekarini Pandjaitan. Menampilkan deskripsi dokter dan motivasi dokter.
- Booking Jadwal, oleh Alif Iqbal Hazairin. Digunakan oleh user untuk memesan jadwal yang tersedia dari seorang dokter.
- Artikel, oleh Abdurrahman Arfan. Artikel dimasukan dalam admin, setelah itu ditampilkan dalam website.

Pipeline status: [![pipeline status](https://gitlab.com/SunnySani/tk_d12/badges/master/pipeline.svg)](https://gitlab.com/SunnySani/tk_d12/-/commits/master)