from django.contrib import admin
from .models import Doctor, Day, Hour, Booking
# Register your models here.

admin.site.register(Day)
admin.site.register(Hour)
admin.site.register(Booking)
