# Generated by Django 3.1.2 on 2020-10-29 11:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0002_auto_20201022_1832'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='image'),
        ),
        migrations.AlterField(
            model_name='tipsandtrick',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='image'),
        ),
    ]
