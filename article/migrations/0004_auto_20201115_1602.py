# Generated by Django 3.1.2 on 2020-11-15 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('article', '0003_auto_20201029_1840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='news',
            name='content',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='tipsandtrick',
            name='content',
            field=models.TextField(default=''),
        ),
    ]
