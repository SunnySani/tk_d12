from django.db import models
from tinymce.models import models as tinymce_models, HTMLField

# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=255, default="")
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def __str__(self):
        return self.title

class TipsAndTrick(models.Model):
    title = models.CharField(max_length=255, default="")
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def __str__(self):
        return self.title