from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *
import datetime

tgl = datetime.date(2000,12,19)
# Create your tests here.
class TestViews(TestCase):
    
    def setUp(self):
        a = News.objects.create(
            title= 'a',
            date= tgl,
            content = 'a',
            image = 'a.png'
        )
        
        b = TipsAndTrick.objects.create(
            title= 'a',
            date= tgl,
            content = 'a',
            image = 'a.png'
        )

    def test_article_url_exists(self):
        response = Client().get(reverse('article:article'))
        self.assertEquals(response.status_code, 200)
    
    def test_article_using_article_template(self):
        response = Client().get(reverse('article:article'))
        self.assertTemplateUsed(response, 'article.html')

    def test_article_using_article_function(self):
        found = resolve('/article/')
        self.assertEqual(found.func, article)

    def test_news_form_url_exists(self):
        response = Client().get(reverse('article:news', args=['1']))
        self.assertEquals(response.status_code, 200)
    
    def test_tips_and_trick_form_url_exists(self):
        response = Client().get(reverse('article:tipsandtrick', args=['1']))
        self.assertEquals(response.status_code, 200)

    def test_model_can_create_news(self):
        counting_all_available_article = News.objects.all().count()
        self.assertEqual(counting_all_available_article, 1)

    def test_model_can_create_tips_and_trick(self):
        counting_all_available_article = TipsAndTrick.objects.all().count()
        self.assertEqual(counting_all_available_article, 1)

    def test_html_contain_any_mandatory_words(self):
        response = Client().get(reverse('article:article'))
        html_kembalian = response.content.decode('utf8')
        self.assertIn('NEWS', html_kembalian)
        self.assertIn('TIPS AND TRICK', html_kembalian)

class TestModels(TestCase):
    def test_artikel_model_str_method(self):
        a = News(title="MILAN SCUDETTO MUSIM INI AAMIIN")
        self.assertEqual(str(a), "MILAN SCUDETTO MUSIM INI AAMIIN")

    def test_tips_and_trick_model_str_method(self):
        a = TipsAndTrick(title="TIPS AND TRICK SUPAYA JAGO SEPERTI MAMANG IBRAHIMOVIC")
        self.assertEqual(str(a), "TIPS AND TRICK SUPAYA JAGO SEPERTI MAMANG IBRAHIMOVIC")