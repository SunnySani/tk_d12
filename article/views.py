from django.shortcuts import render
from .models import News, TipsAndTrick

# Create your views here.
def article(request):
    news = News.objects.all()
    tips = TipsAndTrick.objects.all()
    context = {
        "news": news,
        "tips": tips
    }
    return render(request, "article.html", context)

def news(request, id):
    news_detail = News.objects.get(id=id)
    context = {
        'news_detail': news_detail,
    }
    return render(request, 'news.html', context)

def tipsandtrick(request, id):
    tips_detail = TipsAndTrick.objects.get(id=id)
    context = {
        'tips_detail' : tips_detail
    }
    return render(request, 'tipsandtrick.html', context)
