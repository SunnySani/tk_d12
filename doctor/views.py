from django.shortcuts import render
from .models import Doctor


def DetailDoctor(request, id):
	context= {}
	context["data"] = Doctor.objects.get(id = id)
	
	return render(request, 'doctor/detailDoctor.html', context)

def ListDoctor(request):
	doctors = Doctor.objects.all()
	doctorsList = []
	for i in doctors:
		doctorsList.append(i)

	context = {'doctorsList': doctorsList}
	return render(request, 'doctor/listDoctor.html', context)
