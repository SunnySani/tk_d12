from django import forms
from .models import Suggestions

class SuggestionForm(forms.Form):

	name_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Name'
	}

	email_attrs = {
		'class' : 'form-control',
		'required' : True,
		'placeholder' : 'E-mail'
	}

	suggestion_attrs = {
		'class' : 'form-control',
		'type' : 'text',
		'required' : True,
		'placeholder' : 'Write your suggestion here..'
	}


	name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs=name_attrs))
	email = forms.EmailField(label="", max_length=50, widget=forms.EmailInput(attrs=email_attrs))
	suggestion = forms.CharField(label="", widget=forms.Textarea(attrs=suggestion_attrs))