from django.test import TestCase, Client
from .models import Suggestions
from django.apps import apps
from .apps import HomeConfig
from django.contrib.messages import get_messages

class TestHome(TestCase):
	def test_if_home_url_exist(self):
		response = Client().get('/')
		self.assertEquals(response.status_code, 200)

	def test_if_home_url_uses_the_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home/index.html')

	def test_if_suggestions_model_exist(self):
		Suggestions.objects.create(name="nama", email="namaku@gmail.com", suggestion="ini suggestion")
		num_of_suggestions = Suggestions.objects.all().count()
		self.assertEquals(num_of_suggestions, 1)

	def test_if_home_url_saved_the_data_from_suggestion_form(self):
		Client().post('/', {'name': 'nama', 'email': 'namaku@gmail.com', 'suggestion': 'ini suggestion'})
		num_of_suggestions = Suggestions.objects.all().count()
		self.assertEquals(num_of_suggestions, 1)

	def test_if_success_alert_shows_when_suggestion_form_valid(self):
		response = Client().post('/', {'name': 'nama', 'email': 'namaku@gmail.com', 'suggestion': 'ini suggestion'})
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn('Your suggestion has been submitted. Thank you.', messages)

	def test_if_error_alert_shows_when_suggestion_form_valid(self):
		response = Client().post('/', {'name': 'nama', 'email': 'namaku@apa', 'suggestion': 'ini suggestion'})
		messages = [m.message for m in get_messages(response.wsgi_request)]
		self.assertIn('Invalid suggestion form. Please re-check your e-mail and try again.', messages)

	def test_if_OUR_DOCTORS_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("OUR DOCTORS", html_response)

	def test_if_BOOK_CONSULTATION_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("BOOK CONSULTATION", html_response)

	def test_if_HELP_US_IMPROVE_OUR_PAGE_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("HELP US IMPROVE OUR PAGE", html_response)

	def test_app_name(self):
		self.assertEquals(HomeConfig.name, 'home')
		self.assertEquals(apps.get_app_config('home').name, 'home')

	def test_home_model_str_method(self):
		html_response = Suggestions(name="uvuvevwevwe onyetenyevwe ugwemubwem osas")
		self.assertEqual(str(html_response), "uvuvevwevwe onyetenyevwe ugwemubwem osas")
