from django.shortcuts import render, redirect
from .forms import SuggestionForm
from .models import Suggestions
from article.models import News, TipsAndTrick
from itertools import chain
from operator import attrgetter
from django.contrib import messages

def index(request):
	isvalid = True
	if request.method=='POST':
		form = SuggestionForm(request.POST)
		if form.is_valid():
			data = Suggestions()
			data.name = form.cleaned_data['name']
			data.email = form.cleaned_data['email']
			data.suggestion = form.cleaned_data['suggestion']
			data.save()
			messages.success(request, "Your suggestion has been submitted. Thank you.")
			return redirect('/')
		else:
			messages.error(request, "Invalid suggestion form. Please re-check your e-mail and try again.")			
			isvalid = False

	form = SuggestionForm()
	news = News.objects.all()
	tnt = TipsAndTrick.objects.all()
	data = sorted(
       chain(news, tnt),
       key=attrgetter('date'), reverse=True)

	response = {'form':form, 'data':data, 'news':news, 'tnt':tnt, 'isvalid':isvalid}
	return render(request, 'home/index.html', response)
